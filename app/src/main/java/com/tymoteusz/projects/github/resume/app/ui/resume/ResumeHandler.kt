package com.tymoteusz.projects.github.resume.app.ui.resume

import android.content.Intent
import android.widget.ImageView

interface ResumeHandler {
    fun handleIntent(intent: Intent?)
    fun terminateApiService()
    fun goToUserWebsite()
}