package com.tymoteusz.projects.github.resume.app.ui.resume

import android.content.Intent
import android.net.Uri
import android.util.Log
import com.tymoteusz.projects.github.resume.app.ui.api.GitHubApiService
import com.tymoteusz.projects.github.resume.app.ui.api.UserModel
import com.tymoteusz.projects.github.resume.app.ui.application.Keys
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import android.widget.ImageView
import com.squareup.picasso.Picasso
import com.tymoteusz.projects.github.resume.app.ui.application.ApplicationContext


class ResumePresenter(resumeViewer: ResumeViewer) : ResumeHandler {
    private val gitHubApiService by lazy {
        GitHubApiService.create()
    }
    private var disposable: Disposable? = null
    private var resumeViewer = ResumeWeakViewer(resumeViewer)
    private lateinit var websiteUrl: String

    override fun handleIntent(intent: Intent?) {
        val userId = intent!!.getStringExtra(Keys.USER_NICK_NAME)
        getUserFromService(userId)
    }

    override fun terminateApiService() {
        if (disposable?.isDisposed == false)
            disposable?.dispose()
    }

    override fun goToUserWebsite() {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(websiteUrl))
        ApplicationContext.getAppContext()?.startActivity(browserIntent)
    }

    private fun getUserFromService(userId: String) {
        disposable =
            gitHubApiService.getUser(userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { result ->
                        Log.d(Keys.TAG, "Result $result")
                        changeViewWithNewModel(result)
                    },
                    { error -> Log.d(Keys.TAG, "Error " + error.localizedMessage) }
                )
    }

    private fun changeViewWithNewModel(result: UserModel) {
        val nickname = result.login
        val name = result.name.toString()
        val city = result.location.toString()
        val followers = result.followers.toString()
        val imageUrl = result.avatar_url.toString()
        val email = result.email.toString()
        val company = result.company.toString()
        websiteUrl = result.html_url

        resumeViewer.setTextToViews(nickname, name, city, followers, imageUrl, email, company)
        resumeViewer.hiderLoaderAndShowDetails()
    }
}
