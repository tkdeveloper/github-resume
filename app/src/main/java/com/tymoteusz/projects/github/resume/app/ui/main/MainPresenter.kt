package com.tymoteusz.projects.github.resume.app.ui.main

import android.content.Intent
import com.tymoteusz.projects.github.resume.app.ui.application.ApplicationContext
import com.tymoteusz.projects.github.resume.app.ui.application.Keys
import com.tymoteusz.projects.github.resume.app.ui.resume.ResumeActivity

class MainPresenter(weakViewer: MainViewer) : MainActivityHandler {
    var mainView = MainWeakViewer(weakViewer)

    override fun getInfoAboutUser(userNickname: String) {
        if (userNickname == "") {
            mainView.askToTypeNickName()
            return
        }

        if (!ApplicationContext.isNetworkAvailable())
            return

        goToNextActivity(userNickname)
    }

    private fun goToNextActivity(userNickname: String) {
        val resumeIntent = Intent(ApplicationContext.getAppContext(), ResumeActivity::class.java)
        resumeIntent.putExtra(Keys.USER_NICK_NAME, userNickname)
        ApplicationContext.getAppContext()?.startActivity(resumeIntent)
    }
}