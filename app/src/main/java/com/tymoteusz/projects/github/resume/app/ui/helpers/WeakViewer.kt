package com.tymoteusz.projects.github.resume.app.ui.helpers

import java.lang.ref.WeakReference

abstract class WeakViewer<T>(viewer: T) {

    private val weakReference = WeakReference(viewer)

    protected fun getStronReference(): T? {
        return weakReference.get()
    }
}