package com.tymoteusz.projects.github.resume.app.ui.resume

interface ResumeViewer {
    fun hiderLoaderAndShowDetails()
    fun setTextToViews(
        nickname: String,
        name: String,
        city: String,
        followers: String,
        imageUrl: String,
        email: String,
        company: String
    )
}