package com.tymoteusz.projects.github.resume.app.ui.api

data class UserModel(
    val login: String, val html_url: String, val location: String?,
    val avatar_url: String?, val name: String?, val followers: String?,
    val email: String?, val company: String?
)