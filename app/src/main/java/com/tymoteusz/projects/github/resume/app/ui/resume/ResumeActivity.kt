package com.tymoteusz.projects.github.resume.app.ui.resume

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.squareup.picasso.Picasso
import com.tymoteusz.projects.github.resume.app.R
import kotlinx.android.synthetic.main.activity_resume.*

class ResumeActivity : AppCompatActivity(), ResumeViewer {
    private lateinit var resumePresenter: ResumePresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initWidgets()
        initPresenter()
        manageIntent()
    }

    override fun onStop() {
        super.onStop()
        resumePresenter.terminateApiService()
    }

    override fun hiderLoaderAndShowDetails() {
        AVLoadingIndicatorView.hide()
        setLayoutVisible()
    }

    override fun setTextToViews(
        nickname: String,
        name: String,
        city: String,
        followers: String,
        imageUrl: String,
        email: String,
        company: String
    ) {
        nicknameTv.text = nickname
        nameTV.text = name
        cityTv.text = city
        followersTv.text = followers
        emailTv.text = email
        companyNameTv.text = company

        Picasso.with(this)
            .load(imageUrl)
            .resize(250, 250)
            .centerCrop()
            .into(avatarIm)
    }

    private fun initWidgets() {
        setContentView(R.layout.activity_resume)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        websiteBt.setOnClickListener {
            resumePresenter.goToUserWebsite()
        }
    }

    private fun initPresenter() {
        resumePresenter = ResumePresenter(this)
    }

    private fun manageIntent() {
        resumePresenter.handleIntent(intent)
    }

    private fun setLayoutVisible() {
        nicknameTv.visibility = View.VISIBLE
        nameTV.visibility = View.VISIBLE
        cityTv.visibility = View.VISIBLE
        textView6.visibility = View.VISIBLE
        textView8.visibility = View.VISIBLE
        textView10.visibility = View.VISIBLE
        websiteBt.visibility = View.VISIBLE
        avatarIm.visibility = View.VISIBLE
    }
}
