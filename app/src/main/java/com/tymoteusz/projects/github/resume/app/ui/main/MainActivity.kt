package com.tymoteusz.projects.github.resume.app.ui.main

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.tymoteusz.projects.github.resume.app.R
import com.tymoteusz.projects.github.resume.app.ui.helpers.OneToast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), MainViewer {

    private lateinit var mainActivityPresenter: MainActivityHandler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initPresenter()
        initWidgets()
    }

    override fun askToTypeNickName() {
        val txt = resources.getString(R.string.main_activity_type_nick)
        OneToast.show(txt)
    }

    private fun initWidgets() {
        setContentView(R.layout.activity_main)

        findUserBT.setOnClickListener {
            val userNickname = userNickNameET.text.toString()
            mainActivityPresenter.getInfoAboutUser(userNickname)
        }
    }

    private fun initPresenter() {
        mainActivityPresenter = MainPresenter(this)
    }
}
