package com.tymoteusz.projects.github.resume.app.ui.application

import android.content.Context
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.preference.PreferenceManager
import android.util.Log
import android.support.multidex.MultiDexApplication
import com.tymoteusz.projects.github.resume.app.R
import com.tymoteusz.projects.github.resume.app.ui.helpers.OneToast
import java.lang.ref.WeakReference

class ApplicationContext : MultiDexApplication() {

    companion object {

        private lateinit var weakReference: WeakReference<Context>

        fun getAppContext(): Context? {
            return weakReference.get()
        }

        fun getSharedPreferences(): SharedPreferences? {
            val context = getAppContext() ?: return null
            return PreferenceManager.getDefaultSharedPreferences(context)
        }

        fun log(string: String) {
            Log.d(Keys.TAG, string)
        }

        fun isNetworkAvailable(): Boolean {
            val connectivityManager =
                getAppContext()?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
            val activeNetworkInfo = connectivityManager?.activeNetworkInfo
            val connection = activeNetworkInfo != null && activeNetworkInfo.isConnected
            if (!connection)
                OneToast.show(R.string.noInternet)
            return connection
        }
    }

    override fun onCreate() {
        super.onCreate()
        weakReference = WeakReference(applicationContext)
    }
}
