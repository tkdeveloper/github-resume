package com.tymoteusz.projects.github.resume.app.ui.resume

import com.tymoteusz.projects.github.resume.app.ui.helpers.WeakViewer

class ResumeWeakViewer(viewer: ResumeViewer) : WeakViewer<ResumeViewer>(viewer), ResumeViewer {
    override fun hiderLoaderAndShowDetails() {
        getStronReference()!!.hiderLoaderAndShowDetails()
    }

    override fun setTextToViews(
        nickname: String,
        name: String,
        city: String,
        followers: String,
        imageUrl: String,
        email: String,
        company: String
    ) {
        getStronReference()!!.setTextToViews(nickname, name, city, followers, imageUrl, email, company)
    }
}