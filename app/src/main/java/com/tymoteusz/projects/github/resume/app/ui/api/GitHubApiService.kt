package com.tymoteusz.projects.github.resume.app.ui.api

import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import io.reactivex.Observable


interface GitHubApiService {

    @GET("/users/{userLogin}")
    fun getUser(@Path("userLogin") userNickName: String): Observable<UserModel>

    companion object {
        fun create(): GitHubApiService {
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(
                    RxJava2CallAdapterFactory.create()
                )
                .addConverterFactory(
                    GsonConverterFactory.create()
                )
                .baseUrl(Endpoints.GITHUB_SERVICE_URL)
                .build()

            return retrofit.create(GitHubApiService::class.java)
        }
    }
}