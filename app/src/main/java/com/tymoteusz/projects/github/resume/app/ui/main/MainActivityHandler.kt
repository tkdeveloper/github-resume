package com.tymoteusz.projects.github.resume.app.ui.main

interface MainActivityHandler {
    fun getInfoAboutUser(userNickname: String)
}