package com.tymoteusz.projects.github.resume.app.ui.api

object Endpoints {
    const val GITHUB_SERVICE_URL = "https://api.github.com/"
}