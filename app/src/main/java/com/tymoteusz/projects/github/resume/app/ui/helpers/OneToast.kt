package com.tymoteusz.projects.github.resume.app.ui.helpers

import android.widget.Toast
import com.tymoteusz.projects.github.resume.app.ui.application.ApplicationContext

object OneToast {
    private var toast: Toast? = null

    fun show(text: String, duration: Int) {
        toast?.cancel()
        val context = ApplicationContext.getAppContext() ?: return
        toast = Toast.makeText(context, text, duration)
        toast?.show()
    }

    fun show(text: String) {
        toast?.cancel()
        val context = ApplicationContext.getAppContext() ?: return
        toast = Toast.makeText(context, text, Toast.LENGTH_SHORT)
        toast?.show()
    }

    fun show(textId: Int, duration: Int) {
        toast?.cancel()
        val context = ApplicationContext.getAppContext() ?: return
        toast = Toast.makeText(context, context.getString(textId), duration)
        toast?.show()
    }

    fun show(textId: Int) {
        toast?.cancel()
        val context = ApplicationContext.getAppContext() ?: return
        toast = Toast.makeText(context, context.getString(textId), Toast.LENGTH_SHORT)
        toast?.show()
    }
}
