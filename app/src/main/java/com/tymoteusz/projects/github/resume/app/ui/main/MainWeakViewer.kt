package com.tymoteusz.projects.github.resume.app.ui.main

import com.tymoteusz.projects.github.resume.app.ui.helpers.WeakViewer

class MainWeakViewer(viewer: MainViewer) : WeakViewer<MainViewer>(viewer), MainViewer {
    override fun askToTypeNickName() {
        getStronReference()!!.askToTypeNickName()
    }
}